
// author Hadi Badawi
// CS3307a
// implementation of FileMang.hpp
// implementing the file manager class


#include<stdlib.h>
#include<iostream>
#include<fstream>
#include<string>
#include<string.h>
#include<sys/stat.h>
#include<sys/types.h>
#include<vector>
#include<time.h>
#include<stdio.h>
#include<unistd.h>
#include<errno.h>
#include<pwd.h>
#include<grp.h>
#include <dirent.h>
#include<cstring>

#include "FileMang.hpp"

// constructor for the file manager class, where we get
// as much information from stat function
// i did not have enough time to add the try catch to catch for
// potential errors
FileManager::FileManager(std::string *fileName){
		struct stat tempStat;
		stat(fileName->c_str(), &tempStat);
		this->filename = *fileName;
		//fileName.copy(this.filename, filename.length(),0);
		this->type = tempStat.st_mode >> 9;
		this->fileSize = tempStat.st_size;
		this->userID = tempStat.st_uid;
		this->userName = getpwuid(this->userID)->pw_name;
		this->groupID = tempStat.st_gid;
		this->groupName = getgrgid(this->groupID)->gr_name;
		this->permissions = tempStat.st_mode & 1023;
		this->accessTime = tempStat.st_atim;
		this->modTime = tempStat.st_mtim;
		this->statusChangeTime = tempStat.st_ctim;
		this->blockSize = tempStat.st_blksize;
	}

//destructor for filemanager class, using the default method is fine
FileManager::~FileManager() = default;

	// the broken dump method, the goal of this method is to dump the contents
	// into the filestream pointer provided and close the file
	// again forgot to provide error code return and update the errnum variable
	int FileManager::Dump(std::fstream *strem) {
		std::fstream tempStream;
		tempStream.open(this->filename, std::fstream::in | std::fstream::out);

        	if (tempStream){
   	        	char * tempBuf = new char [this->blockSize];
       			while (tempStream.read(tempBuf, this->blockSize)){
                		(*strem) << tempStream.gcount();
                		std::cout << tempStream.gcount();
            		}

        	}
		tempStream.close();
        	return 0;
   	}

	// the rename method takes the string pointer name of the newname
	// and updates it accordingly
	// again no catch for errors, did not have enough time
    	int FileManager::Rename(std::string *newName){
        	const char * temp = (this->filename).c_str();
	        this->filename = temp;
        	return rename(temp, newName->c_str()); // returns 0 if successful
	}

	// the remove method removes the file from the system and destroyes
	// the destructor method
	// no error catch here
    	int FileManager::Remove(){
		unlink(this->filename.c_str());
		(*this).~FileManager();
        	return 0;
	}

	// the compare method takes a string name of the other file
	// compares it with our file and turns whether or not the
	// the contents of the files are the same or not
	// not the differences or where they occur
	// no errors caught here again
    	int FileManager::Compare(std::string *otherFile){
        	std::fstream tempStream;
        	tempStream.open(this->filename, std::fstream::in);
        	std::fstream tempStr2;
        	tempStr2.open(*otherFile, std::fstream::in);

        	if (tempStream && tempStr2){
            		char * tempBuf = new char [this->blockSize];
            		char * tempBuf2 = new char [this->blockSize];

       			while (tempStream.read(tempBuf, this->blockSize) &&
       				tempStr2.read(tempBuf2, this->blockSize)){
   		             	if (tempBuf != tempBuf2){
 	                   	return 1;
        	        	}
            		}

        	}
        	return 0;
	}

	// the Expand method simply expands the subfiles vector
	// by expanding the file if it is a directory
	// and storing the files as strings in the vector
	// does return an err num
  	int FileManager::Expand(){
		if (this->type == 1){

			struct dirent * currDir;
            		DIR *dirPointer;

      		      	dirPointer = opendir(this->filename.c_str());
	
	            	if (dirPointer == NULL){
        	        	return 1;
            		}

       		     	while ((currDir = readdir(dirPointer)) != NULL){
                		this->subfiles.push_back(currDir->d_name);
            		}

            		closedir(dirPointer);
            		return 0;
		}

		else {
	            this->errnum = ENOTSUP;
        	    return errnum;
		}
	}


	// getter for filename
	std::string FileManager::getName(){
        	return this->filename;
    	}

	// getter for type
    	int FileManager::getType(){
   	     return this->type;
  	  }

	// getter for file size
   	int FileManager::getFilesize(){
        	return (int) this->fileSize;
    	}

	// getter for user id
    	int FileManager::getUserID(){
        	return (int)this->userID;
    	}

	// getter for user name
    	std::string FileManager::getUserName(){
        	return this->userName;
    	}

	// getter for group id
    	int FileManager::getGroupID(){
        	return (int)this->groupID;
    	}

	// getter for group name
    	std::string FileManager::getGroupName(){
        	return this->groupName;
    	}

	// getter for permissions
    	std::string FileManager::getPermissions(){
        	return this->permissions;
    	}

	// getter for access time
    	struct timespec FileManager::getAccessTime(){
        	return this->accessTime;
    	}

	// getter for modified time
    	struct timespec FileManager::getModTime(){
        	return this->modTime;
    	}

	// getter for status change time
    	struct timespec FileManager::getStatusChangeTime(){
        	return this->statusChangeTime;
   	}

	// getter for block size
    	int FileManager::getBlockSize(){
        	return this->blockSize;
    	}

	// getter for children in directory
    	std::vector<std::string> FileManager::getSubFiles(){
        	return this->subfiles;
    	}

	// getter for error number
    	int FileManager::getErrorNum(){
        	return this->errnum;
    	}

	// getter for error number but in string
    	std::string FileManager::getErrorNumStr(){
        	return strerror(this->errnum);
    	}

	// a function to get a string that has every piece of information about a file
	// from filemanager class
    	std:: string FileManager::getStat() {
        	return this->getName() + " " + std::to_string(this->getType())+ " " + std::to_string((this)->getFilesize())+ " " + std::to_string((this)->getUserID()) + " " +
		this->getUserName()+ " " +std::to_string(this->getGroupID())+ " " +  this->getGroupName()+ " " + this->getPermissions() + " " +
    		std::to_string(this->getBlockSize());
    	// did not include this since i can't convert time to string
    	//this->getAccessTime() + " " + this->getModTime() + " " + this->getStatusChangeTime()+ "\n"

    	}

	// this method was supposed to help and convert the time to a string to be 
	// returned with getStat, but i could not figure it out in time
   	/* void FileManager::correctTime(char * temp , struct timespec *tem1){
        	std::timespec ts;
       	 std::timespec_get(tem1, TIME_UTC);
      	  char buf[100];
       	 std::strftime(buf, sizeof buf, "%D %T", std::gmtime(&ts.tv_sec));
      	  std::printf("Current time: %s.%09ld UTC\n", buf, ts.tv_nsec);
	
    	}*/
;


