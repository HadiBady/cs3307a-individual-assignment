// author Hadi Badawi
// CS3307a
// myrm which implements the same thing as ls in unix system

#include<iostream>
#include<fstream>
#include<string>
#include<sys/stat.h>
#include<vector>
#include<time.h>
#include<stdio.h>
#include<unistd.h>
#include<cstring>
#include <utility>

#include"FileMang.hpp"

// checks to make sure it is current dir or not, then
// lists the files in the dir
int main(int argc, char *argv[]){

	// checks to make sure argument has 1 parameter
	if (argc == 2){
        //storing that parameter
        std::string temp = argv[1];
        // checks for -l command
		if (temp == "-l" ) {
                temp = "."; // so store the current dire as a file of .
            	if ( temp.size() != 0){
                    FileManager *file1 = new FileManager(&temp);

                	std::vector<std::string> temp2;

                	temp2 = file1->getSubFiles();

                	// if we get back a vector that is not empty then it is a
                    // dir otherwise invalid operation
                    // and print file names with a for loop
                	if (temp.size() != 0){
                    		for (std::string i : temp2){
                        		FileManager * tem = new FileManager (&i);
                        		std::cout << tem->getStat() << std::endl; // print the details of each file
                    		}
                    }
                    else{
                        std::cout << "not a valid operation on a non directory file" << std::endl;
                    }

                }
        // need a case to catch everything else
        else if (temp.length() != 0) {
            std::string temp = argv[1];
			FileManager *file1 = new FileManager(&temp);

                std::vector<std::string> temp2;

                temp2 = file1->getSubFiles();

                // if we get back a vector that is not empty then it is a
                // dir otherwise invalid operation
                if (temp2.size() != 0){
                    for (std::string i : temp2){
                        FileManager * tem = new FileManager (&i);
                        std::cout << tem->getStat() << std::endl;
                    }
                    return 0;
                }
                std::cout << file1->getName() << std::endl; // printing just the names of each file
			}


        }
	}
    // case for when you only write ls, then simply list current dir, similar
    // to previous else if but with current dir set as .
	else if (argc == 1){
        std::string temp = ".";
			FileManager *file1 = new FileManager(&temp);

                std::vector<std::string> temp2;
                temp2 = file1->getSubFiles();

                if (temp2.size() != 0){
                    for (std::string i : temp2){
                        FileManager * tem = new FileManager (&i);
                        std::cout << tem->getStat() << std::endl;
                    }
                    return 0;
                }
	}
	// catch the case where invalid num of arguments is given
	else {

		std::cout << "Cannot ls command, invalid number of arguments" << std::endl;
	}
}
