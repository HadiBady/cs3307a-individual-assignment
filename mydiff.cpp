// author Hadi Badawi
// CS3307a
// mydiff which implements the same thing as diff/compare (i don't know) in unix system

#include<iostream>
#include<fstream>
#include<string>
#include<sys/stat.h>
#include<vector>
#include<time.h>
#include<stdio.h>
#include<unistd.h>
#include<cstring>

#include"FileMang.hpp"


// This follows a similar structure to mycp, execpt that
// instead of copying the file it looks for differences
int main(int argc, char *argv[]){
    	int i = 1;
	int imax = 0;

	if (argc == 3){
		std::string temp = argv[1];
		FileManager currFile (&temp);
		std::string temp2 = argv[2];
        	int result;

		result = currFile.Compare(&temp2);

		if (result == 1){
            		std::cout << "There are no differences" << std::endl;
		}
		std::cout << "They are different files" << std::endl;
	}

	else {
		std::cout << "Cannot compare files, invalid number of arguments" << std::endl;
		return 1;
	}


};
