// author Hadi Badawi
// CS3307a
// mycat which implements the same thing as cat in unix system



#include<iostream>
#include<fstream>
#include<string>
#include<sys/stat.h>
#include<vector>
#include<time.h>
#include<stdio.h>
#include<unistd.h>
#include<cstring>

#include"FileMang.hpp"


int main(int argc, char *argv[]){

	int i = 1; // this is used to determine the start number for arguments
	int imax = 0; // this is used to determine the final number of arguments in the arguments array

	// when larger than 2 or equal to, then max arguments is the arguments given in the command
	if (argc >= 2){
		imax = argc;
	}
	
	// when argument number is not greater than 1, then that means not enough arguments for cat
	else {
		std::cout << "Cannot copy file, invalid number of arguments" << std::endl;
		return 1;
	}

	// this for loop makes sure it performs cat for all files in the arguments
	// by first making a file manager object with the name of file from command arguments 
	// dumped to output file which will be dumped to iostream
	// because a file is created in a process we remove it after.
	for (; i < imax; i++){
		std::fstream output;
		output.open("tempFile", std::fstream::out|std::fstream::in);
		std::string temp = argv[i];
		FileManager currFile (&temp);
		currFile.Dump(&output);
		output.flush();
		std::cout << std::endl;
		unlink("tempFile");
    }
}

