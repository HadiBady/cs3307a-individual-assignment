// author Hadi Badawi
// CS3307a
// myrm which implements the same thing as rm (i don't know) in unix system

#include<iostream>
#include<fstream>
#include<string>
#include<sys/stat.h>
#include<vector>
#include<time.h>
#include<stdio.h>
#include<unistd.h>

#include"FileMang.hpp"

// This follows a similar structure to mydiff, execpt that
// instead of getting the differences between files it recursively 
// removes the files specificed in the command line arguments
int main(int argc, char *argv[]){
	int i = 1; // starting point
	int imax = 0;	// max number of arguments
	if (argc >= 2){
		imax = argc;
	}

	else {
		std::cout << "Cannot remove file(s), invalid number of arguments" << std::endl;
		return 1;
	}

	for (; i < imax; i++){
        std::string temp = argv[i];
		FileManager currFile = FileManager(&temp);
		currFile.Remove();
	}
}
