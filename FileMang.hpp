
// author Hadi Badawi
// CS3307a
// the header file for the FileMang.cpp file



#ifndef FILEMANG_HPP
#define FILEMANG_HPP

#include<iostream>
#include<fstream>
#include<string>
#include<vector>
#include<time.h>
#include<cstring>


// definition of the class File Manager
// this is how we are going to store the information about the file

class FileManager {
	private:
		std::string filename;
		int type;
		long int fileSize;
		uid_t userID;
		std::string userName;
		gid_t groupID;
        	std::string groupName;
		std::string permissions; // this is takend from mode_t parameter
		struct timespec accessTime;
		struct timespec modTime;
		struct timespec statusChangeTime;
		blksize_t blockSize;
		std::vector<std::string> subfiles;
		int errnum;

		// this is supposed to convert the time to a string which one can understand
		void correctTime(char *, struct timespec *);

	public:
		// constructor for the file manager class, where we get
		// as much information from stat function
		FileManager(std::string* );

		//destructor for filemanager class
		~FileManager();

		// the rename method takes the string pointer name of the newname
		// and updates it accordingly
		int Rename(std::string *);

		// the broken dump method, the goal of this method is to dump the contents
		// into the filestream pointer provided and close the file
		int Dump(std::fstream *);

		// the remove method removes the file from the system and destroyes
		// the destructor method
		int Remove();

		// the compare method takes a string name of the other file
		// compares it with our file and turns whether or not the
		// the contents of the files are the same or not
		// the differences or where they occur
		int Compare(std::string *);

		// the Expand method simply expands the subfiles vector
		// by expanding the file if it is a directory
		// and storing the files as strings in the vector
		int Expand();


		/** the rest are getters and setters for the class **/
		std::string getName();

		int getType();

   		int getFilesize();

    		int getUserID();

    		std::string getUserName();

    		int getGroupID();

    		std::string getGroupName();

    		std::string getPermissions();

    		struct timespec getAccessTime();

    		struct timespec getModTime();

    		struct timespec getStatusChangeTime();

    		int getBlockSize();

    		std::vector<std::string> getSubFiles();

    		int getErrorNum();

    		std::string getErrorNumStr();

    		std::string getStat();

};

#endif // FILE_MANG_HPP
