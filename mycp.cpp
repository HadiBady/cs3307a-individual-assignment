// author Hadi Badawi
// CS3307a
// mycp which implements the same thing as cp in unix system

#include<iostream>
#include<fstream>
#include<string>
#include<sys/stat.h>
#include<vector>
#include<time.h>
#include<stdio.h>
#include<unistd.h>
#include<cstring>

#include "FileMang.hpp"



int main(int argc, char *argv[]){
	// checks to make sure we have 2 parameters to which we can copy
	// the fontents of one file into another
	if (argc == 3){

		// this will store the first file name
        	std::string temp = argv[1];

		FileManager file1 (&temp); // creating the file manager object

		std::fstream tempStr; // creating the fstream to have the contents of the file dumped there

		tempStr.open(argv[2], std::fstream::out|std::fstream::in); 

		(file1).Dump(&tempStr); // dumping the contents of file 1 into file 2

		tempStr.close();
	}
	// not enough or too many arguments we through this statment to streamos
	else {
        	std::cout << "Cannot copy file, invalid number of arguments" << std::endl;
    	}
};
