// author Hadi Badawi
// CS3307a
// mystat which implements the same thing as stat (i don't know) in unix system but
// with only the things in the filemanager class

#include<iostream>
#include<fstream>
#include<string>
#include<sys/stat.h>
#include<vector>
#include<time.h>
#include<stdio.h>
#include<unistd.h>
#include<cstring>

#include"FileMang.hpp"

//Checks to make sure you have 1 parameter
// then simply calls the getStat() function which 
// prints all the relevent information of a file
int main(int argc, char *argv[]){
	if (argc == 2){
        std::string temp = argv[1];

		FileManager file1 = FileManager(&temp);

		std::cout << file1.getStat() << std::endl;
	}
	else {
	std::cout << "Cannot copy file, invalid number of arguments" << std::endl;
    }
}
