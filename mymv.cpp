// author Hadi Badawi
// CS3307a
// mymv which implements the same thing as mv (i don't know) in unix system

#include<iostream>
#include<fstream>
#include<string>
#include<sys/stat.h>
#include<vector>
#include<time.h>
#include<stdio.h>
#include<unistd.h>

#include"FileMang.hpp"

// it takes in 2 parameters and moves the first file to the name of the second
// essentially renames the file to the second parameter
// very similar structure to mydiff, myrm, mycp
int main(int argc, char *argv[]){
    int i = 1;
	int imax = 0;

	if (argc == 3){
		std::string temp = argv[i];
		FileManager currFile = FileManager(&temp);
		std::string temp2 = argv[2];
		currFile.Rename( &temp2);
	}

	else {
		std::cout << "Cannot move file, invalid number of arguments" << std::endl;
		return 1;
	}


}
